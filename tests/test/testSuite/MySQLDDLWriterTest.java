/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testSuite;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Hassan J. Ndow
 */
public class MySQLDDLWriterTest {
    public Object mockTableObjectExists(){
        Object obj = new Object();
        return obj;
    }
    
    public Object mockTableObjectNull(){
        Object obj = null;
        return obj;
    }
    
    //Case 1 - No ORM Table object passed
    @Test 
    public void testDontCreateDDL() {
        
        Assert.assertNull(mockTableObjectNull());
        System.out.println("Case 1 - No valid table object passed");
        return;
        
    }
    
    //Case 2 - ORM Table object passed
    @Test 
    public void testCreateDDL() {
        
        Assert.assertNotNull(mockTableObjectExists());
        String ddl = "Case 2 - MySQL DDL statements";
        System.out.println(ddl);
        
        
    }
}
