/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Hassan J. Ndow
 */

@RunWith(Suite.class)				
@Suite.SuiteClasses({				
  EdgeReaderTest.class,
  XMLReaderTest.class,
  GenericReaderTest.class,
  MySQLDDLWriter.class,
  OracleDDLWriter.class,
  GenericDDLWriter.class,
  
})
public class SuiteTest {
    
}
