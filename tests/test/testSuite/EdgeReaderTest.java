
package testSuite;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.internal.runners.statements.ExpectException;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author Hassan J. Ndow
 */



public class EdgeReaderTest {
    
   
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    
    //Case 1 - No file entered
    @Test 
    public void testReadMissingFile() throws FileNotFoundException {
        
        thrown.expect(FileNotFoundException.class);
        File file = new File("src/test.edg");
        InputStream inputStream = new FileInputStream(file); 
        
        
    }
    
    //Case 2 - incorrect file format
    @Test 
    public void testReadInvalidFile()   {
        
        //thrown.expect(ParseException.class);
        File file = new File("src/test.xml");
        String format = file.getName().substring(file.getName().lastIndexOf(".")
        + 1, file.getName().length());
        if(!format.equals("edg") && format.length() > 0) {
            System.out.println("Case 2: incorrect file extension - should be .edg");
            return;
        }
        assertNotEquals("Valid format extension - edg","edg", format);
    }
    
    //Case 3 - incorrect file format
//    @Test 
//    public void testReadMalformedFile()   {
//        
//    }
    
    //Case 4 - valid file
    @Test 
    public void testReadValidFile()   {
        
        //thrown.expect(ParseException.class);
        File file = new File("src/test2.edg");
        String format = null;
        if(file.exists() && !file.isDirectory()){
            format = file.getName().substring(file.getName().lastIndexOf(".")
            + 1, file.getName().length());
        }
        assertEquals("Invalid format extension - not an edg file","edg", format);
    }
    
    
   
    
}
