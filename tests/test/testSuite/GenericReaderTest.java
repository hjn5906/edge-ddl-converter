/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testSuite;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 *
 * @author Hassan J. Ndow
 */
public class GenericReaderTest {
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    
    //Case 1 - No file entered
    @Test 
    public void testReadMissingFile() throws FileNotFoundException {
        
        thrown.expect(FileNotFoundException.class);
        File file = new File("src/test.generic");
        InputStream inputStream = new FileInputStream(file); 
        
        
    }
    
    //Case 2 - incorrect file format
    @Test 
    public void testReadInvalidFile()   {
        
        //thrown.expect(ParseException.class);
        File file = new File("src/test.edg");
        String format = file.getName().substring(file.getName().lastIndexOf(".")
        + 1, file.getName().length());
        if(!format.equals("generic") && format.length() > 0) {
            System.out.println("Case 2: incorrect file extension - should be .generic");
            return;
        }
        assertNotEquals("Valid format extension - generic","generic", format);
    }
    
    //Case 3 - incorrect file format
//    @Test 
//    public void testReadMalformedFile()   {
//        
//    }
    
    //Case 4 - valid file
    @Test 
    public void testReadValidFile()   {
        
        //thrown.expect(ParseException.class);
        File file = new File("src/test2.generic");
        String format = null;
        if(file.exists() && !file.isDirectory()){
            format = file.getName().substring(file.getName().lastIndexOf(".")
            + 1, file.getName().length());
        }
        assertEquals("Invalid format extension - not an generic file","generic", format);
    }
}
